﻿namespace WslToolbox.Gui.Configurations
{
    public class AppearanceConfiguration
    {
        public ThemeConfiguration.Styles SelectedStyle { get; set; } = ThemeConfiguration.Styles.Auto;
    }
}