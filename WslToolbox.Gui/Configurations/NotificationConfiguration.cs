﻿namespace WslToolbox.Gui.Configurations
{
    public class NotificationConfiguration
    {
        public bool Enabled { get; set; } = true;
        public bool NewVersionAvailable { get; set; } = true;
    }
}